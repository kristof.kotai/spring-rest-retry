package com.example.demo.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@RestController
public class TomatoResource {

//    @Retryable(maxAttempts=6)
    @Retryable
    @GetMapping(path="/tomato/{id}")
    public ResponseEntity<String> tomato(@PathVariable int id) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")
                .withZone(ZoneId.systemDefault());
        
        Instant instant = Instant.now();

        System.out.println(String.format("/tomato/%s @ %s", id, formatter.format( instant )));

        if (id == 1) {
            throw new Exception("RETRY ME!");
        }

        return new ResponseEntity<String>(
            String.format("Hello user ID: %s", id),
            HttpStatus.OK
        );
    }
}
